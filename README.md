<div align='center'>

  # Alacritty

</div>

## 📦 Dependencies (Arch)

### ❗ Required

Official :

```shell
paru -S alacritty
```

### ➕ Optional

GIT :

```shell
cd ~/.config
git clone --filter blob:none --sparse https://github.com/ryanoasis/nerd-fonts

cd nerd-fonts
git sparse-checkout add patched-fonts/Lilex

./install.sh Lilex
```
